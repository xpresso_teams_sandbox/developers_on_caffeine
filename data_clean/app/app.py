"""
This is the implementation of data preparation for sklearn
"""
import os
import joblib
import sys
import logging
import jsonmerge
import requests
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
import validators
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score

__author__ = "### Author ###"

logger = XprLogger("data_clean", level=logging.INFO)


class DataClean(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, path1, path2):
        super().__init__(name="DataClean")
        """ Initialize all the required constansts and data her """
        self.clean_data = pd.read_csv(path1, parse_dates=['Date'])
        self.tempearature_data = pd.read_csv(path2)
        self.unique_country = []
        self.clean_data_1 = []
        # self.global_temp_country_clear = []
        self.mean_temp = None
        self.resultant = None
        self.model = LinearRegression()

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        self.clean_data = self.clean_data.rename(columns={'Country/Region': 'Country', 'Province/State': 'Province'})
        self.clean_data.isnull().sum()
        self.clean_data = self.clean_data.drop_duplicates(keep='first')
        self.clean_data.info()
        self.tempearature_data.isnull().sum()
        self.tempearature_data = self.tempearature_data.drop_duplicates(keep='first')
        self.clean_data_1 = self.clean_data.groupby(['Country', 'Date'], as_index=False).sum()
        self.unique_country = self.clean_data_1.Country.unique()

        self.global_temp_country_clear = self.tempearature_data[~self.tempearature_data['Country'].isin(
            ['Denmark', 'Antarctica', 'France', 'Europe', 'Netherlands', 'United Kingdom', 'Africa', 'South America'])]

        self.global_temp_country_clear = self.global_temp_country_clear.replace(
            ['Denmark (Europe)', 'France (Europe)', 'Netherlands (Europe)', 'United Kingdom (Europe)'],
            ['Denmark', 'France', 'Netherlands', 'United Kingdom'])

        self.mean_temp = []
        for country in self.unique_country:
            self.mean_temp.append(
                self.global_temp_country_clear[self.global_temp_country_clear['Country'] ==
                                               country]['AverageTemperature'].mean())

        d = {'Country': self.unique_country, 'temp': self.mean_temp}
        d_dataframe = pd.DataFrame(d)
        d_dataframe = d_dataframe.drop_duplicates(keep='first')
        self.resultant = self.clean_data_1.merge(d_dataframe, on='Country', how='left')
        basedate = pd.Timestamp('2020-1-22')
        self.resultant['days'] = (self.resultant['Date'] - basedate).dt.days
        self.resultant.Country = pd.Categorical(self.resultant.Country)
        self.resultant['Country_Code'] = self.resultant.Country.cat.codes
        self.resultant = self.resultant[~self.resultant['Country'].isin(['Diamond Princess', 'MS Zaandam'])]
        self.resultant.corr(method='pearson')
        self.resultant.corr(method='kendall')
        self.completed()

    def model(self):
        resultant = self.resultant[[col for col in self.resultant.columns if col != 'Confirmed'] + ['Confirmed']]
        resultant.head()
        X = resultant.iloc[:, 7:10].values
        y = resultant.iloc[:, 10].values
        # splitting dataset into training and testing dataset
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1 / 5, random_state=0)
        self.model.fit(X_train, y_train)
        # Predicting the Test set results
        y_pred = self.model.predict(X_test)
        loss = mean_squared_error(y_pred, y_test)
        logging.info(loss)
        self.send_metrics("loss", loss)

    def send_metrics(self, status, result):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """

        report_status = {
            "status": {"status": status},
            "metric": {"accuracy": result}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.
        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """

        logging.info("Saving model")
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        path = os.path.join(self.OUTPUT_DIR, 'linear_model_1.pkl')
        joblib.dump(self.model, path)
        logging.info("Saved model... load model")
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = DataClean(
        path1="/data/clean_data/covid_19_clean_complete_till8.csv",
        path2="/data/clean_data/GlobalLandTemperaturesByCountry.csv")

    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
